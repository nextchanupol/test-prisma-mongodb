## Test Prisma MongoDB

ทดสอบติดตั้งและใช้งาน prisma ร่วมกับ MongoDB (เผื่อจะกลับมาดูในวันที่ต้องใช้งาน)

#### Getting Started รวบรัด

1. npm init -y
2. ในที่นี้จะใช้งาน typescript npm install prisma typescript ts-node @types/node --save-dev
3. สร้างไฟล์ tsconfig.json

```
{
  "compilerOptions": {
    "sourceMap": true,
    "outDir": "dist",
    "strict": true,
    "lib": ["esnext"],
    "esModuleInterop": true
  }
}
```

4. npx prisma init
   4.1 ใน folder schema/schema.prisma

   ```
   generator client {
     provider        = "prisma-client-js"
     previewFeatures = ["mongoDb"] // เพิ่มบรรทัดนี้
   }
   datasource db {
     provider = "mongodb" // เปลี่ยนจาก ==>  provider = "postgresql"
     url      = env("DATABASE_URL")
   }

   // สร้าง model ตามสบายใจเลย

   model User {
     id        String   @id @default(dbgenerated()) @map("_id") @db.ObjectId
     createdAt DateTime @default(now())
     email     String   @unique
     name      String?
     role      Role     @default(USER)
     posts     Post[]
   }
   model Post {
     id        String   @id @default(dbgenerated()) @map("_id") @db.ObjectId
     createdAt DateTime @default(now())
     updatedAt DateTime @updatedAt
     published Boolean  @default(false)
     title     String
     author    User?    @relation(fields: [authorId], references: [id])
     authorId  String   @db.ObjectId
   }
   enum Role {
     USER
     ADMIN
   }
   ```

5. npx prisma generate
6. สร้างไฟล์ index.ts
```

    import { PrismaClient } from "@prisma/client";

    const prisma = new PrismaClient();

    async function main() {
      await prisma.$connect();

      await prisma.user.create({
        data: {
          name: `Chanupol.p`,
          email: "ouu@naja.com",
          posts: {
            create: {
              title: "My first post",
            },
          },
        },
      });

      const allUsers = await prisma.user.findMany({});
      console.dir(allUsers, { depth: null });

      const [posts, totalPosts] = await prisma.$transaction([
        prisma.post.findMany({ where: { title: { contains: "prisma" } } }),
        prisma.post.count(),
      ]);


      console.dir(posts, { depth: null });
      console.dir(totalPosts, { depth: null });
    }

    main()
      .catch((e) => {
        throw e;
      })
      .finally(async () => {
        await prisma.$disconnect();
      });

```


7. npx ts-node index.ts
   7.1
   ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) `ถ้าเจอ error แบบนี้ ==> Error occurred during query execution: ConnectorError(ConnectorError { user_facing_error: None, kind: RawError { code: "unknown", message: "Command failed (IllegalOperation): This MongoDB deployment does not support retryable writes. Please add retryWrites=false to your connection string.)" } })`

ใช้วิธีแก้คือ ไปสร้าง repl set โดยดูตัวอย่างและคำอธิบายได้ที่ช่อง [CMDev](https://www.youtube.com/watch?v=cL7aNR4-Gso) ของอาจารย์เล็กครับ

##### _แหล่งข้อมูล_

- [Prisma](https://www.prisma.io/)
- [database-connectors](https://www.prisma.io/docs/concepts/database-connectors/mongodb)
- [mongodb-typescript-mongodb](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/mongodb-typescript-mongodb)

