import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
  await prisma.$connect();

  await prisma.user.create({
    data: {
      name: `Rich2`,
      email: "hello@prisma.com2",
      posts: {
        create: {
          title: "My first post 2",
        },
      },
    },
  });

  const allUsers = await prisma.user.findMany({});
  console.dir(allUsers, { depth: null });

  const [posts, totalPosts] = await prisma.$transaction([
    prisma.post.findMany({ where: { title: { contains: "prisma" } } }),
    prisma.post.count(),
  ]);

  
  console.dir(posts, { depth: null });
  console.dir(totalPosts, { depth: null });
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
